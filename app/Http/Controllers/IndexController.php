<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(){

        $client = new Client();
        $response = $client->get('http://www.guiatrabalhista.com.br/guia/salario_minimo.htm');
        $content = $response->getBody()->getContents();
        $dom = new \DOMDocument;

        @$dom->loadHTML($content);
        $dom->preserveWhiteSpace = false;
        $tables = $dom->getElementsByTagName('table');

        $rows = $tables->item(0)->getElementsByTagName('tr');

        $salarios = [];

        foreach ($rows as $index => $row) {

            if($index == 0) continue;

            $cols = $row->getElementsByTagName('td');

            array_push($salarios, [
                'vigencia' => trim($cols[0]->nodeValue),
                'valor_mensal' => trim($cols[1]->nodeValue),
                'valor_diario' => trim($cols[2]->nodeValue),
                'valor_hora' => trim($cols[3]->nodeValue),
                'norma_legal' => trim($cols[4]->nodeValue),
                'dou' => trim($cols[5]->nodeValue)
            ]);
            
        }

        return response()->json($salarios);

    }
}
